package co.com.tcc.maestro.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;

public class Utilidades {

	public static String nullToEmpty(String s) {
		if (s == null || s.equals("null") || s.equals("NULL")) {
			return "";
		}
		return s;
	}

	public static String getStackTrace(Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		if (throwable != null) {
			throwable.printStackTrace(pw);
			return sw.getBuffer().toString();
		}
		return "excepcion nula";
	}

	public static void limpiarMapeoConnection(DataSource dataSource) {

		Connection connection = null;
		/**
		 * Se limpia el mapeo de objetos de la conecci�n, debido a que queda en
		 * cach� y genera error al usarlo con otras interfaces que utilicen la misma
		 * conecci�n, por ej, javax.sql.Datasource o clases como JdbcTemplate.
		 * 
		 * Adem�s los servicios de wm hacen esto y crean un cach� global a nivel de
		 * datasource.
		 */
		try {
			connection = dataSource.getConnection();
			connection.getTypeMap().clear();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			DbUtils.closeQuietly(connection);
		}

	}

	public static List<String> separarStringLista(String listado, String separador) {
		String[] items = listado.split(separador);
		List<String> container = Arrays.asList(items);
		return container;
	}

}
