package co.com.tcc.maestro.dao.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnType;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dto.oracle.ObjDatosCursorMovilizar;
import co.com.tcc.maestro.dto.oracle.ObjMensajes;
import co.com.tcc.maestro.exception.ServiceException;
import co.com.tcc.maestro.model.Formulario;
import co.com.tcc.maestro.model.Movilizacion;
import oracle.jdbc.OracleTypes;

@Repository
public class MovilizarUnidadesLogisticasDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@SuppressWarnings({ "unused", "unchecked" })
	public List<Movilizacion> MovilizarUnidadesLogisticas(Formulario datosformulario) throws IOException, SQLException {
		List<Movilizacion> respuesta = new ArrayList<Movilizacion>();
		String tiunid = "";
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName("WRAPPER_MOVILIZACION_UNIDADES")
				.withProcedureName("MOVILIZAR_PRC");

		jdbcCall.addDeclaredParameter(new SqlParameter("I_LOCA_ID_INT", OracleTypes.NUMBER));
		jdbcCall.addDeclaredParameter(new SqlParameter("I_UNNE_ID_INT", OracleTypes.NUMBER));
		jdbcCall.addDeclaredParameter(new SqlParameter("I_TILO_ID_INT", OracleTypes.VARCHAR));
		jdbcCall.addDeclaredParameter(new SqlParameter("I_FECINI_PROC", OracleTypes.DATE));
		jdbcCall.addDeclaredParameter(new SqlParameter("I_FECFIN_PROC", OracleTypes.DATE));
		jdbcCall.addDeclaredParameter(new SqlParameter("I_USUARIO", OracleTypes.VARCHAR));
		jdbcCall.declareParameters(new SqlOutParameter("IOREF_UNIDADES_MOV_INV", OracleTypes.CURSOR),
				new SqlOutParameter("IOREF_UNIDADES_MOV_TER", OracleTypes.CURSOR),
				new SqlOutParameter("IOT_MENSAJES", Types.STRUCT, "OBJ_MENSAJES_T", new SqlReturnType() {
					@Override
					public Object getTypeValue(CallableStatement cs, int paramIndex, int sqlType, String typeName)
							throws SQLException {
						Struct item = ((Struct) cs.getObject(paramIndex));
						Object[] arregloOut = item.getAttributes();
						ObjMensajes msg = new ObjMensajes();
						msg.setCodigoSistema(new BigDecimal(arregloOut[0].toString()));
						msg.setMensajeSistema(arregloOut[1] == null ? null : arregloOut[1].toString());
						msg.setCodigoUsuario(new BigDecimal(arregloOut[2].toString()));
						msg.setMensajeUsuario(arregloOut[3] == null ? null : arregloOut[3].toString());
						return msg;
					}
				}));
		jdbcCall.returningResultSet("IOREF_UNIDADES_MOV_INV", new RowMapper<ObjDatosCursorMovilizar>() {

			@Override
			public ObjDatosCursorMovilizar mapRow(ResultSet rs, int rowNum) throws SQLException {

				ObjDatosCursorMovilizar objDatosCursorMovilizar = new ObjDatosCursorMovilizar();

				objDatosCursorMovilizar.setTiloIdInt(rs.getString("ID_TILO_ID_INT"));
				objDatosCursorMovilizar.setTotal(rs.getString("TOTAL"));

				return objDatosCursorMovilizar;
			}
		});
		jdbcCall.returningResultSet("IOREF_UNIDADES_MOV_TER", new RowMapper<ObjDatosCursorMovilizar>() {

			@Override
			public ObjDatosCursorMovilizar mapRow(ResultSet rs, int rowNum) throws SQLException {
				ObjDatosCursorMovilizar objDatosCursorMovilizar = new ObjDatosCursorMovilizar();

				objDatosCursorMovilizar.setTiloIdInt(rs.getString("ID_TILO_ID_INT"));
				objDatosCursorMovilizar.setTotal(rs.getString("TOTAL"));

				return objDatosCursorMovilizar;
			}
		});
		for (int i = 0; i < datosformulario.getTipoUnidadesLogicas().size(); i++) {
			tiunid = datosformulario.getTipoUnidadesLogicas().get(i).getId() + ", " + tiunid;
		}
		tiunid = tiunid.substring(0, tiunid.length() - 2);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("I_LOCA_ID_INT", datosformulario.getCentroOperacion().getId());
		params.put("I_UNNE_ID_INT", datosformulario.getUnidadNegocio().getId());
		params.put("I_TILO_ID_INT", tiunid);
		params.put("I_FECINI_PROC", datosformulario.getFechaInicioProceso());
		params.put("I_FECFIN_PROC", datosformulario.getFechaFinProceso());
		params.put("I_USUARIO", "mespitia"); // UsuarioLogueado.getUsuario()

		// Respuesta de paquete de Oracle
		Map<String, Object> out = jdbcCall.execute(params);

		ObjMensajes mensaje = (ObjMensajes) out.get("IOT_MENSAJES");

		if (!mensaje.getCodigoSistema().equals(BigDecimal.ZERO))
			throw new ServiceException(mensaje.getMensajeSistema());

		if (!mensaje.getCodigoUsuario().equals(BigDecimal.ZERO))
			throw new ServiceException(mensaje.getMensajeUsuario());

		List<ObjDatosCursorMovilizar> listMercaciaInventario = (List<ObjDatosCursorMovilizar>) out
				.get("IOREF_UNIDADES_MOV_INV");
		List<ObjDatosCursorMovilizar> listMercaciaTerminal = (List<ObjDatosCursorMovilizar>) out
				.get("IOREF_UNIDADES_MOV_TER");

		List<ObjDatosCursorMovilizar> listaMercanciaMovilizada = new ArrayList<ObjDatosCursorMovilizar>();
		if (listMercaciaInventario != null && listMercaciaInventario.size() > 0)
			listaMercanciaMovilizada.addAll(listMercaciaInventario);
		if (listMercaciaTerminal != null && listMercaciaTerminal.size() > 0)
			listaMercanciaMovilizada.addAll(listMercaciaTerminal);

		if (listaMercanciaMovilizada != null && listaMercanciaMovilizada.size() > 0) {

			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
			Date now = new Date();
			String strDate = sdfDate.format(now);
			int countCantidadRespuestaTiloidInt = 0;
			System.out.println("Esto es: " + listaMercanciaMovilizada.size());

			for (int i = 0; i < datosformulario.getTipoUnidadesLogicas().size(); i++) {
				Movilizacion objetoRespuesta = new Movilizacion();
				boolean resgistrosSimilares = false;
				objetoRespuesta.setUnidadeNegocio(datosformulario.getUnidadNegocio().getDescripcion());
				objetoRespuesta.setCentroOperacion(datosformulario.getCentroOperacion().getDescripcion());
				objetoRespuesta.setEstado("COMPLETADO");
				objetoRespuesta.setFechaMovimiento(strDate);
				objetoRespuesta.setUsuario("mespitia".toUpperCase());
				for (int j = 0; j < listaMercanciaMovilizada.size(); j++) {

					if (listaMercanciaMovilizada.get(j).getTiloIdInt()
							.equals(datosformulario.getTipoUnidadesLogicas().get(i).getId().toString())) {
						resgistrosSimilares = true;
						objetoRespuesta
								.setTipoUbicacion(datosformulario.getTipoUnidadesLogicas().get(i).getDescripcion());
						objetoRespuesta.setTotalRegistros(listaMercanciaMovilizada.get(j).getTotal());

					}
				}
				if (!resgistrosSimilares) {
					objetoRespuesta.setTipoUbicacion(datosformulario.getTipoUnidadesLogicas().get(i).getDescripcion());
					objetoRespuesta.setTotalRegistros("0");

				}
				respuesta.add(countCantidadRespuestaTiloidInt, objetoRespuesta);
				countCantidadRespuestaTiloidInt = countCantidadRespuestaTiloidInt + 1;
			}

		}

		return respuesta;

	}

}
