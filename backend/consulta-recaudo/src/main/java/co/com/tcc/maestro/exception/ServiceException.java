package co.com.tcc.maestro.exception;

public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = -4325423036600329705L;

	public ServiceException() {

	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}