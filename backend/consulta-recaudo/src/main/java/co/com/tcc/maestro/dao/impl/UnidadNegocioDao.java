package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.rowmapper.UnidadNegocioRowMapper;
import co.com.tcc.maestro.model.ObjetoListas;

@Repository
public class UnidadNegocioDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<ObjetoListas> consultarUnidadNegocio() {

		String sql = "SELECT  TO_CHAR( UNNE_ID_INT ) AS UNNE_ID_INT, \r\n" + 
				"                  UNNE_DESCRIPCION, \r\n" + 
				"                   UNNE_ABREVIATURA \r\n" + 
				"            FROM    MAE_UNIDAD_NEGOCIOS \r\n" + 
				"            WHERE   UNNE_ESTADO = 'A' \r\n" + 
				"            AND UNNE_ID_INT in (UTILI_WRAPPER_SERVICIOS_UTI.FNC_PARAMETROS_SISTEMA('UNNE_PAQ'), UTILI_WRAPPER_SERVICIOS_UTI.FNC_PARAMETROS_SISTEMA('UNNE_MEN')) \r\n" + 
				"            ORDER BY UNNE_DESCRIPCION ";
		return jdbcTemplate.query(sql, new UnidadNegocioRowMapper());
	}
}
