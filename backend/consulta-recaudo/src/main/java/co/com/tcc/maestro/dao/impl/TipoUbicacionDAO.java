package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.rowmapper.TipoUbicacionRowMapper;
import co.com.tcc.maestro.model.ObjetoListas;

@Repository
public class TipoUbicacionDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<ObjetoListas> consultarTipoUbicacion() {

		String sql = " SELECT LIVA_VALOR, LIVA_DESCRIPCION, LIVA_ABREVIATURA\r\n" + 
					 " FROM MAE_LISTA_VALORES \r\n" + 
					 " WHERE LIST_ID_INT IN (UTILI_WRAPPER_SERVICIOS_UTI.FNC_PARAMETROS_SISTEMA('ID_LIST_UBIC_PLAT'), UTILI_WRAPPER_SERVICIOS_UTI.FNC_PARAMETROS_SISTEMA('ID_LIST_UBIC_PLS'))\r\n" + 
					 " AND LIVA_VALOR != UTILI_WRAPPER_SERVICIOS_UTI.FNC_PARAMETROS_SISTEMA('VALOR_UBIC_INVENTARIOS')\r\n" + 
					 " ORDER BY LIVA_DESCRIPCION ASC";
		return jdbcTemplate.query(sql, new TipoUbicacionRowMapper());
	}

}
