package co.com.tcc.maestro.utils;

/**
 * Clase que contiene el nombre de los par�metros de sistema
 * 
 * @author Joan Roa
 *
 */
public class Parametros {

	/**
	 * Iniciales que identifica a la aplicaci�n RE = registro. C = Courier
	 */
	public static final String PREFIJO_USUARIO_APLICACION = "REC";

	public static final String UNIDADES_NEGOCIO_MALLAS = "UNIDADES_NEGOCIO_MALLAS";

	public static final String ID_LISTA_TIPO_CUENTAS = "LISTA_TIPO_CUENT_NAC";

	/**
	 * ID LISTA MEDIOS DE DISTRIBUCION
	 */
	public static final String LISTA_MED_LOG_REG = "LISTA_MED_LOG_REG";

	/**
	 * Medios de distribuci�n que requieren documentaci�n.
	 */
	public static final String MEDIOS_DIST_REQ_DOC = "MEDIOS_DIST_REQ_DOC";

	/**
	 * Id Tipo Medio Contacto Telefono Celular
	 */
	public static final String ID_TIPO_CONTACTO_CEL = "TIPO_CONT_TELE_CELU";

	/**
	 * Id Tipo Medio Contacto Email Operativo
	 */
	public static final String ID_TIPO_CONTACTO_EMAIL = "TIPO_CONT_EMAIL_OPER";

	/**
	 * Id del tipo de direccion residencia
	 */

	public static final String TIPO_DIR_RESIDENCIA = "TIPO_DIR_RESIDENCIA";

	/**
	 * Id del Rol Aliado Log�stico
	 */
	public static final String ROL_TERC_ALIA_LOGIS = "ROL_TERC_ALIA_LOGIS";

	/**
	 * IDENTIFICACION DEL ESTADO DE DISPONIBILIDAD QUE APLICA SOLO PARA EL OBJETO DE
	 * NEGOCIO VEHICULO (MAE_ESTADO_OBJETO_NEGOCIO)
	 */
	public static final String VHCL_DISPO = "VHCL_DISPO";

	/**
	 * Valor del Estado del registro para VEHICULOS al registrarse por el modulo
	 * REGISTRO COURIER
	 */
	public static final String ESTADO_REG_VEHI_REC = "ESTADO_REG_VEHI_REC";

	/**
	 * Id del TIPO DE OPERACION por defecto al registrar veh�culos desde el
	 * m�dulo courier (REGISTRO_COURIER)
	 */
	public static final String TIPO_OPE_VEHI_REC = "TIPO_OPE_VEHI_REC";

	/**
	 * Id de la unidad de negocio por defecto al registrar veh�culos desde el
	 * m�dulo courier (REGISTRO_COURIER)',
	 */
	public static final String UNNE_ID_VEHI_REC = "UNNE_ID_VEHI_REC";

	/**
	 * Medios de distribucion que se registran en MAE_VEHICULOS Y MAE_SOAT_VEHICULO
	 */
	public static final String MEDIOS_DIST_MAE_VEHICULOS = "MEDIOS_DIST_MAE_VEHICULOS";

	/**
	 * Valor del estado del registro MEDIO_LOGISTICO por defecto al registrarse por
	 * el modulo REGISTRO COURIER
	 */
	public static final String ESTADO_REG_MELO_REC = "ESTADO_REG_MELO_REC";

	/**
	 * Carpeta del S3 donde se almacenan los documentos adjuntos desde el modulo
	 * consulta-recaudo
	 */
	public static final String CARPETA_DOC_REC = "CARPETA_DOC_REC";

	/**
	 * BUCKETNAME PARA TRAER LOS ARCHIVOS DE consulta-recaudo S3
	 */
	public static final String BUCKETNAME_S3_ARCHIVOS = "BUCKETNAME_S3_ARCHIVOS";

	/**
	 * ID_INT de los roles separados por , para validar en registro courier,
	 * terceros que sean empleados tcc.
	 */
	public static final String TIPO_ROLES_EMPLEADOS_REC = "TIPO_ROLES_EMPLEADOS_REC";

	/**
	 * Valor del MEDIO DISTRIBUCI�N BICICLETA
	 */
	public static final String MED_DIST_BICICLETA = "MED_DIST_BICICLETA";

	/**
	 * Valor del MEDIO DISTRIBUCI�N A PIE
	 */
	public static final String MED_DIST_PIE = "MED_DIST_PIE";

	/**
	 * ID_INT DEL TIPO DE VEH�CULO BICICLETA
	 */
	public static final String TIPO_VEHI_BICI = "TIPO_VEHI_BICI";

	/**
	 * Mae parametro sistema texto tratamiento de datos.
	 */
	public static final String AUTORIZA_TRATA_DATOS = "AUTORIZA_TRATA_DATOS";

	/**
	 * Mae parametro sistema tipo de fotos dmplataforma.
	 */
	public static final String LISTA_CLASE_FOTOS_MOVILIDAD = "LISTA_CLASE_FOTOS_MOVILIDAD";

	/**
	 * IDENTIFICADORES DE LOS PROCESOS PARA UNIDADES CONTENEDORAS EN DMPLATAFORMA
	 */
	public static final String PROCESOS_UC_DMPLATAFORMA = "PROCESOS_UC_DMPLATAFORMA";

	/**
	 * ID LISTA LIVAR QUE ESPECIFICA LA CLASE DE TIPO UC TEMPORAL
	 */
	public static final String VALOR_CLASE_UC_TEMPORAL = "VALOR_CLASE_UC_TEMPORAL";

}
