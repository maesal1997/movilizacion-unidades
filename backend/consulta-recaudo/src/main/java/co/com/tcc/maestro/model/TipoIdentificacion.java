package co.com.tcc.maestro.model;

public class TipoIdentificacion {

	private int id;

	private String descripcion;

	public TipoIdentificacion() {
		// TODO Auto-generated constructor stub
	}

	public TipoIdentificacion(int id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
