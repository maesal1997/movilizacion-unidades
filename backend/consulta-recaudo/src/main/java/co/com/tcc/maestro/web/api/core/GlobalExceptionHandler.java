package co.com.tcc.maestro.web.api.core;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.amazonaws.services.cloudfront.model.AccessDeniedException;

import co.com.tcc.maestro.dto.core.Respuesta;
import co.com.tcc.maestro.exception.DaoException;
import co.com.tcc.maestro.exception.ServiceException;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler({ ServiceException.class })
	public Respuesta handleServiceException(Exception e, HttpServletResponse response) {
		response.setStatus(400);
		Respuesta respuesta = new Respuesta();
		respuesta.setMensaje(e.getMessage());
		return respuesta;
	}

	@ExceptionHandler({ DaoException.class })
	public Respuesta handleDaoException(Exception e, HttpServletResponse response) {
		response.setStatus(400);
		Respuesta respuesta = new Respuesta();
		respuesta.setMensaje(e.getMessage());
		return respuesta;
	}

	@ExceptionHandler({ AccessDeniedException.class })
	public Respuesta AccessDeniedException(Exception e, HttpServletResponse response) {

		response.setStatus(400);
		Respuesta respuesta = new Respuesta();
		respuesta.setMensaje("Acceso denegado");
		return respuesta;
	}

	@ExceptionHandler({ Exception.class })
	public Respuesta handleExceptions(Exception e, HttpServletResponse response, WebRequest webRequest) {

		response.setStatus(500);
		e.printStackTrace();
		Respuesta respuesta = new Respuesta();
		respuesta.setMensaje("Ocurri� un error durante el proceso: " + e.getMessage());
		respuesta.setException(e.getMessage());
		return respuesta;
	}

	@ExceptionHandler({ RuntimeException.class })
	public Respuesta runtimeException(RuntimeException e, HttpServletResponse response) {

		response.setStatus(500);
		e.printStackTrace();
		Respuesta respuesta = new Respuesta();
		respuesta.setMensaje("Ocurri� un error durante el proceso: " + e.getMessage());
		respuesta.setException(e.getMessage());
		return respuesta;
	}

}
