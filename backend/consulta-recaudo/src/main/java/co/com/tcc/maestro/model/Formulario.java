package co.com.tcc.maestro.model;

import java.sql.Timestamp;
import java.util.List;

public class Formulario {

	private ObjetoListas centroOperacion;
	private ObjetoListas unidadNegocio;
	private List<ObjetoListas> tipoUnidadesLogicas;
	private Timestamp fechaInicioProceso;
	private Timestamp fechaFinProceso;

	public ObjetoListas getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(ObjetoListas centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public ObjetoListas getUnidadNegocio() {
		return unidadNegocio;
	}

	public void setUnidadNegocio(ObjetoListas unidadNegocio) {
		this.unidadNegocio = unidadNegocio;
	}

	public List<ObjetoListas> getTipoUnidadesLogicas() {
		return tipoUnidadesLogicas;
	}

	public void setTipoUnidadesLogicas(List<ObjetoListas> tipoUnidadesLogicas) {
		this.tipoUnidadesLogicas = tipoUnidadesLogicas;
	}

	public Timestamp getFechaInicioProceso() {
		return fechaInicioProceso;
	}

	public void setFechaInicioProceso(Timestamp fechaInicioProceso) {
		this.fechaInicioProceso = fechaInicioProceso;
	}

	public Timestamp getFechaFinProceso() {
		return fechaFinProceso;
	}

	public void setFechaFinProceso(Timestamp fechaFinProceso) {
		this.fechaFinProceso = fechaFinProceso;
	}

}
