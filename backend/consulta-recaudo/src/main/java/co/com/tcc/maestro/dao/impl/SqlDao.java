package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.exception.DaoException;
import co.com.tcc.maestro.service.Messages;

@Repository
public class SqlDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private Messages messages;

	public Object obtenerValor(String sql, Object parametro, Class<?> clase) {

		List<?> valores = null;
		if (parametro == null)
			valores = jdbcTemplate.queryForList(sql, clase);
		else
			valores = jdbcTemplate.queryForList(sql, new Object[] { parametro }, clase);

		if (valores.isEmpty()) {

			throw new DaoException(messages.get("sql.dao.noresultados"));

		}
		if (valores.size() > 1) {

			throw new DaoException(messages.get("sql.dao.sobrantes"));

		}

		return valores.get(0);
	}

}
