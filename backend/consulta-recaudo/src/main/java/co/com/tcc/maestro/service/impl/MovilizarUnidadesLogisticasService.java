package co.com.tcc.maestro.service.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.impl.MovilizarUnidadesLogisticasDao;
import co.com.tcc.maestro.model.Formulario;
import co.com.tcc.maestro.model.Movilizacion;

@Service
public class MovilizarUnidadesLogisticasService {

	@Autowired
	private MovilizarUnidadesLogisticasDao movilizarUnidadesLogisticasDao;

	public List<Movilizacion> MovilizarUnidadesLogisticas(Formulario datosformulario) throws IOException, SQLException {
		return movilizarUnidadesLogisticasDao.MovilizarUnidadesLogisticas(datosformulario);
	}

}
