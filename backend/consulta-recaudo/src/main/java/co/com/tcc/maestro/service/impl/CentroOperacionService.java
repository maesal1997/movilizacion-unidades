package co.com.tcc.maestro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.impl.CentroOperacionDAO;
import co.com.tcc.maestro.model.ObjetoListas;

@Service
public class CentroOperacionService {

	@Autowired
	private CentroOperacionDAO centroOperacionDAO;

	public List<ObjetoListas> consultarCentrosOperacion() {

		return centroOperacionDAO.consultarCentrosOperacion();
	}
}
