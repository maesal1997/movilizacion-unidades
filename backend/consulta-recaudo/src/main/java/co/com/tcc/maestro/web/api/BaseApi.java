package co.com.tcc.maestro.web.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.tcc.maestro.model.ObjetoListas;
import co.com.tcc.maestro.service.impl.CentroOperacionService;
import co.com.tcc.maestro.service.impl.CodigoUbicacionService;
import co.com.tcc.maestro.service.impl.ParametroService;
import co.com.tcc.maestro.service.impl.TipoUbicacionService;
import co.com.tcc.maestro.service.impl.UnidadNegocioService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class BaseApi {


	private static final String api = "api/base/";

	@Autowired
	private CentroOperacionService centroOperacionService;
	
	@Autowired
	private UnidadNegocioService unidadNegocioService;
	
	@Autowired
	private ParametroService parametroService;
	
	@Autowired
	private TipoUbicacionService tipoUbicacionService;
	
	@Autowired
	private CodigoUbicacionService codigoUbicacionService;

	@RequestMapping(value = api + "centros/operacion", method = RequestMethod.GET)
	public List<ObjetoListas> consultarCentrosOperacion() {

		return centroOperacionService.consultarCentrosOperacion();
	}
	
	@RequestMapping(value = api + "unidad/negocio", method = RequestMethod.GET)
	public List<ObjetoListas> consultarUnidadNegocio() {

		return unidadNegocioService.consultarUnidadNegocio();
	}
	
	@RequestMapping(value = api + "tipo/ubicacion", method = RequestMethod.GET)
	public List<ObjetoListas> consultarTipoUbicacion() {

		return tipoUbicacionService.consultarTipoUbicacion();
	}
	
	@PostMapping(value = api + "codigo/ubicacion")
	public List<ObjetoListas> consultarCodigoUbicacion(
			@RequestParam String tipoLocalizacion, 
			@RequestParam String ceop) {

		return codigoUbicacionService.consultarCodigoUbicacion(tipoLocalizacion, ceop);
	}
	
	
	@GetMapping(value = api + "all")
	public Map<String, Object> listarTodos() {

		Map<String, Object> todo = new HashMap<String, Object>();
		todo.put("listaCentrosOperacion", centroOperacionService.consultarCentrosOperacion());
		todo.put("listaUnidadNegocio",  unidadNegocioService.consultarUnidadNegocio());
		todo.put("listaTipoUbicacion",  tipoUbicacionService.consultarTipoUbicacion());
		return todo;
	}

	@PostMapping(value = api + "parametros")
	public Map<String, Object> consultarParametros(@RequestBody List<String> nombresParametros) {

		return parametroService.consultarParametros(nombresParametros);
	}

}
