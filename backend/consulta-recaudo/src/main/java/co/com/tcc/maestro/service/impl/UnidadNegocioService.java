package co.com.tcc.maestro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.impl.UnidadNegocioDao;
import co.com.tcc.maestro.model.ObjetoListas;

@Service
public class UnidadNegocioService {
   
	@Autowired
	private UnidadNegocioDao unidadNegocioDao;

	public List<ObjetoListas> consultarUnidadNegocio() {

		return unidadNegocioDao.consultarUnidadNegocio();
	}
}
