package co.com.tcc.maestro.utils;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.Subject;

import co.com.tcc.maestro.exception.ServiceException;
import co.com.tcc.maestro.model.Persona;
import weblogic.security.Security;
import weblogic.security.spi.WLSGroup;
import weblogic.security.spi.WLSUser;

public class UsuarioLogueado {
	public static String getUsuario() {
		Subject subject = Security.getCurrentSubject();
		for (Principal principal : subject.getPrincipals()) {
			if ((principal instanceof WLSUser)) {
				return ((WLSUser) principal).getName();
			}
		}
		throw new ServiceException("Usuario no Autenticado");

	}

	public static boolean hasRol(String rol) {
		String rolWbl;

		Subject subject = Security.getCurrentSubject();
		for (Principal principal : subject.getPrincipals()) {
			if (principal instanceof WLSGroup) {
				rolWbl = (((WLSGroup) principal).getName());
				if (rolWbl.equals(rol)) {
					return true;
				}
			}

		}
		return false;
	}

	public static List<Object> listarRoles() {

		List<Object> roles = new ArrayList<Object>();

		String rolWbl;
		Subject subject = Security.getCurrentSubject();
		for (Principal principal : subject.getPrincipals()) {
			if (principal instanceof WLSGroup) {
				rolWbl = (((WLSGroup) principal).getName());
				roles.add(rolWbl);
			}

		}

		return roles;
	}

	public static String generarUsuarioNoLogueado(Persona persona) {

		String usuario = "";

		if (persona.getPrimerNombre() == null)
			throw new ServiceException("");

		if (persona.getPrimerNombre().length() < 2)
			throw new ServiceException("");

		if (persona.getPrimerNombre().length() == 2)
			usuario = Parametros.PREFIJO_USUARIO_APLICACION + persona.getPrimerNombre().substring(0, 2).toUpperCase();

		if (persona.getPrimerNombre().length() > 2)
			usuario = Parametros.PREFIJO_USUARIO_APLICACION + persona.getPrimerNombre().substring(0, 3).toUpperCase();

		if (persona.getPrimerApellido().length() == 2)
			usuario += persona.getPrimerApellido().substring(0, 2).toUpperCase();

		if (persona.getPrimerApellido().length() > 2)
			usuario += persona.getPrimerApellido().substring(0, 3).toUpperCase();

		return usuario;

	}

}
