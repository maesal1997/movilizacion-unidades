package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.rowmapper.CentroOperacionRowMapper;
import co.com.tcc.maestro.model.ObjetoListas;

@Repository
public class CentroOperacionDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<ObjetoListas> consultarCentrosOperacion() {

		String sql = "SELECT LOCA_ID_INT, LOCA_DESCRIPCION, LOCA_ABREVIATURA FROM MAE_CENTRO_DISTRIBUCION_V";
		sql +=" WHERE LOCA_ESTADO = 'A' ORDER BY LOCA_DESCRIPCION ASC";
		return jdbcTemplate.query(sql, new CentroOperacionRowMapper());
	}
}
