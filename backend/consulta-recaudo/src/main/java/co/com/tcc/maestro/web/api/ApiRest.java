package co.com.tcc.maestro.web.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Clase de Prueba
 * 
 * @author Joan Roa
 *
 */
@RestController
@RequestMapping("api/test")
public class ApiRest {

	@GetMapping()
	public String cosumirServicioAcortarUrl() throws Exception {

		return "test";

	}

}
