package co.com.tcc.maestro.dto.oracle;

public class Mensaje {

	private String mensaje;
	private Object body;
	private String strackTrace;

	/**
	 * @param mensaje
	 */

	public Mensaje() {

	}

	public Mensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Mensaje(String mensaje, String strackTrace, Object body) {
		this.mensaje = mensaje;
		this.strackTrace = strackTrace;
		this.body = body;
	}

	/**
	 * @param mensaje
	 * @param body
	 */

	public Mensaje(String mensaje, Object body) {
		this.mensaje = mensaje;
		this.body = body;
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the body
	 */
	public Object getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(Object body) {
		this.body = body;
	}

	public String getStrackTrace() {
		return strackTrace;
	}

	public void setStrackTrace(String strackTrace) {
		this.strackTrace = strackTrace;
	}

}
