package co.com.tcc.maestro.dto.oracle;

public class ObjDatosCursorMovilizar {

	private String tiloIdInt;
	private String total;

	public String getTiloIdInt() {
		return tiloIdInt;
	}

	public void setTiloIdInt(String tiloIdInt) {
		this.tiloIdInt = tiloIdInt;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

}
