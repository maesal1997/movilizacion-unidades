package co.com.tcc.maestro.web.api.movilizacionUnidades;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.tcc.maestro.model.Formulario;
import co.com.tcc.maestro.model.Movilizacion;
import co.com.tcc.maestro.service.impl.MovilizarUnidadesLogisticasService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MovilizarUnidadesLogisticasApi {
	private static final String api = "api/base/";

	@Autowired
	private MovilizarUnidadesLogisticasService movilizarUnidadesLogisticasService;

	@PostMapping(value = api + "movilizar/Unidades")
	public List<Movilizacion> MovilizarUnidadesLogisticas(@RequestBody Formulario datosformulario)
			throws IOException, SQLException {

		return movilizarUnidadesLogisticasService.MovilizarUnidadesLogisticas(datosformulario);
	}
}
