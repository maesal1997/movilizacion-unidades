package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.rowmapper.CodigoUbicacionRowMapper;
import co.com.tcc.maestro.model.ObjetoListas;

@Repository
public class CodigoUbicacionDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<ObjetoListas> consultarCodigoUbicacion(String tipoLocalizacion, String ceop) {

		String sql = "SELECT  UBTCC.UBTCC_ID_INT AS ID, \r\n" + 
				"           UBTCC.DESCRIPCION AS DESCRIPCION\r\n" + 
				"    			,UBTCC.CODI_BARRAS AS CODI_BARRAS\r\n" + 
				"			 FROM \r\n" + 
				"     ARC_UBICACION_TCC2_VM UBTCC\r\n" + 
				"     INNER JOIN MAE_LOCALIZACION LOCA ON LOCA.LOCA_ID_INT = UBTCC.UBTCC_ID \r\n" + 
				"     WHERE UBTCC.TIUB_ID_INT = UTILI_WRAPPER_SERVICIOS_UTI.FNC_PARAMETROS_SISTEMA('TIPO_UBIC_LOCALIZACION')\r\n" + 
				"     AND UBTCC.LOCA_ID_INT_COP = \r\n" + ceop +
				"     AND LOCA.TILO_ID_INT = \r\n" + tipoLocalizacion +
				"     ORDER BY UBTCC.DESCRIPCION ASC";
		return jdbcTemplate.query(sql, new CodigoUbicacionRowMapper());
	}

}
