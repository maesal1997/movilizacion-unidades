package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.model.ObjetoListas;

public class CodigoUbicacionRowMapper implements RowMapper<ObjetoListas>{

	private String prefijo = "";

	public CodigoUbicacionRowMapper() {
		// TODO Auto-generated constructor stub
	}

	public CodigoUbicacionRowMapper(String prefijo) {
		// TODO Auto-generated constructor stub
		this.prefijo = prefijo;
	}

	public ObjetoListas mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ObjetoListas centroOperacion = new ObjetoListas();

		centroOperacion.setId(rs.getBigDecimal(getPrefijo() + "ID"));
		centroOperacion.setDescripcion(rs.getString(getPrefijo() + "DESCRIPCION"));
		centroOperacion.setAbreviatura(rs.getString(getPrefijo() + "CODI_BARRAS"));

		return centroOperacion;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

}
