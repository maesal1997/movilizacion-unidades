package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.model.ObjetoListas;

public class UnidadNegocioRowMapper implements RowMapper<ObjetoListas> {
	private String prefijo = "";

	public UnidadNegocioRowMapper() {
		// TODO Auto-generated constructor stub
	}

	public UnidadNegocioRowMapper(String prefijo) {
		// TODO Auto-generated constructor stub
		this.prefijo = prefijo;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	public ObjetoListas mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ObjetoListas unidadNegocio = new ObjetoListas();

		unidadNegocio.setId(rs.getBigDecimal(getPrefijo() + "UNNE_ID_INT"));
		unidadNegocio.setDescripcion(rs.getString(getPrefijo() + "UNNE_DESCRIPCION"));
		unidadNegocio.setAbreviatura(rs.getString(getPrefijo() + "UNNE_ABREVIATURA"));

		return unidadNegocio;
	}
}
