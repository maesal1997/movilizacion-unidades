package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.model.Parametro;

public class ParametroRowMapper implements RowMapper<Parametro> {

	@Override
	public Parametro mapRow(ResultSet rs, int rowNum) throws SQLException {

		Parametro parametro = new Parametro();
		parametro.setNombre(rs.getString("PARA_NOMBRE"));
		parametro.setValor(rs.getString("PARA_VALOR"));
		return parametro;
	}

}
