package co.com.tcc.maestro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.impl.CodigoUbicacionDAO;
import co.com.tcc.maestro.model.ObjetoListas;

@Service
public class CodigoUbicacionService {
	
	@Autowired
	private CodigoUbicacionDAO codigoUbicacionDAO;

	public List<ObjetoListas> consultarCodigoUbicacion(String tipoLocalizacion, String ceop) {

		return codigoUbicacionDAO.consultarCodigoUbicacion(tipoLocalizacion, ceop);
	}

}
