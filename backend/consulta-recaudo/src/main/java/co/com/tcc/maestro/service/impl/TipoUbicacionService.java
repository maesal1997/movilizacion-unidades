package co.com.tcc.maestro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.impl.TipoUbicacionDAO;
import co.com.tcc.maestro.model.ObjetoListas;

@Service
public class TipoUbicacionService {
	
	@Autowired
	private TipoUbicacionDAO tipoUbicacionDAO;

	public List<ObjetoListas> consultarTipoUbicacion() {

		return tipoUbicacionDAO.consultarTipoUbicacion();
	}

}
