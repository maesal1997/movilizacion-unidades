package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.model.ObjetoListas;

public class TipoUbicacionRowMapper implements RowMapper<ObjetoListas>{

	private String prefijo = "";

	public TipoUbicacionRowMapper() {
		// TODO Auto-generated constructor stub
	}

	public TipoUbicacionRowMapper(String prefijo) {
		// TODO Auto-generated constructor stub
		this.prefijo = prefijo;
	}

	public ObjetoListas mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ObjetoListas tipoUbicacion = new ObjetoListas();

		tipoUbicacion.setId(rs.getBigDecimal(getPrefijo() + "LIVA_VALOR"));
		tipoUbicacion.setDescripcion(rs.getString(getPrefijo() + "LIVA_DESCRIPCION"));
		tipoUbicacion.setAbreviatura(rs.getString(getPrefijo() + "LIVA_ABREVIATURA"));

		return tipoUbicacion;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}
}
