package co.com.tcc.maestro.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.impl.ParametrosDao;
import co.com.tcc.maestro.exception.ServiceException;

@Service
public class ParametroService {

	@Autowired
	private ParametrosDao parametrosDao;

	public String consultarParametro(String parametro) {

		return parametrosDao.consultarParametro(parametro);
	}

	public Object consultarParametro(String parametro, Class<?> clase) {

		return parametrosDao.consultarParametro(parametro, clase);
	}

	public Map<String, Object> consultarParametros(List<String> nombresParametros) {

		if (nombresParametros == null)
			throw new ServiceException("Debe especificar al menos 1 parámetro");

		if (nombresParametros.isEmpty())
			throw new ServiceException("Debe especificar al menos 1 parámetro");

		return parametrosDao.consultarParametros(nombresParametros);
	}

}
